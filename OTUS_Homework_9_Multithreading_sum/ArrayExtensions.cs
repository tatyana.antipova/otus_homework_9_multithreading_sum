﻿namespace OTUS_Homework_9_Multithreading_sum;

internal static class ArrayExtensions
{
    public static long OrdinarySum(this IEnumerable<int> array)
    {
        var result = 0;
        int arrayLength = array.Count();
        for (int i = 0; i < arrayLength; i++)
            result += array.ElementAt(i);

        return result;
    }

    public static long ParallelWithLinqSum(this int[] array)
    {
        return array.AsParallel().Sum();
    }

    public async static Task<long> ParallelSum(this int[] array, int threadCount)
    {
        var parts = array.Split(threadCount);
        
        var tasks = new List<Task<long>>();
        foreach (var part in parts)
        {            
            tasks.Add(Task.Run<long>(() => part.OrdinarySum()));
        }

        Task.WaitAll(tasks.ToArray());

        long result = 0;
        foreach (var item in tasks)
        {
            result += await item;
        }

        return result;
    }

    private static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> list, int parts)
    {
        var bunchLength = list.Count() / parts;
        for (int i = 0; i <= parts; i ++)
        {
            yield return list.Skip(i * bunchLength).Take(bunchLength);
        }
    }
}

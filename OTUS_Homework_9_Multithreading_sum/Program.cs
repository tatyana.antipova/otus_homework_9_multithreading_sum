﻿using System.Diagnostics;
using System.Threading.Tasks;

namespace OTUS_Homework_9_Multithreading_sum;

internal class Program
{
    static async Task Main(string[] args)
    {
        var gen = new DataGenerator();
        var data100k = gen.GetIntArray(length: 100_000);
        var data1m = gen.GetIntArray(length: 1_000_000);
        var data10m = gen.GetIntArray(length: 10_000_000);

        var sw = Stopwatch.StartNew();
        /// OrdinarySum
        Console.WriteLine($"OrdinarySum 100k result: {ArrayExtensions.OrdinarySum(data100k)}. Elapsed {sw.ElapsedMilliseconds}ms");

        sw.Restart();
        Console.WriteLine($"OrdinarySum 1m result: {ArrayExtensions.OrdinarySum(data1m)}. Elapsed {sw.ElapsedMilliseconds}ms");

        sw.Restart();
        Console.WriteLine($"OrdinarySum 10m result: {ArrayExtensions.OrdinarySum(data10m)}. Elapsed {sw.ElapsedMilliseconds}ms");

        /// ParallelWithLinqSum
        sw.Restart();
        Console.WriteLine($"ParallelWithLinqSum 100k result: {ArrayExtensions.ParallelWithLinqSum(data100k)}. Elapsed {sw.ElapsedMilliseconds}ms");

        sw.Restart();
        Console.WriteLine($"ParallelWithLinqSum 1m result: {ArrayExtensions.ParallelWithLinqSum(data1m)}. Elapsed {sw.ElapsedMilliseconds}ms");

        sw.Restart();
        Console.WriteLine($"ParallelWithLinqSum 10m result: {ArrayExtensions.ParallelWithLinqSum(data10m)}. Elapsed {sw.ElapsedMilliseconds}ms");

        /// ParallelSum
        sw.Restart();
        var res1 = await ArrayExtensions.ParallelSum(data100k, 2);
        Console.WriteLine($"ParallelSum 100k result: {res1}. Elapsed {sw.ElapsedMilliseconds}ms");

        sw.Restart();
        var res2 = await ArrayExtensions.ParallelSum(data1m, 2);
        Console.WriteLine($"ParallelSum 1m result: {res2}. Elapsed {sw.ElapsedMilliseconds}ms");

        sw.Restart();
        var res3 = await ArrayExtensions.ParallelSum(data10m, 2);
        Console.WriteLine($"ParallelSum 10m result: {res3}. Elapsed {sw.ElapsedMilliseconds}ms");
    }

   
}
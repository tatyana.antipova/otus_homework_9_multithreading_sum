﻿namespace OTUS_Homework_9_Multithreading_sum;

internal class DataGenerator
{
    private readonly Random _random = new Random();
    private const int _minValue = -1000;
    private const int _maxValue = 1000;

    public int[] GetIntArray(int length)
    { 
        var result = new int[length];
        for (int i = 1; i < length; i++)
        {
            result[i] = _random.Next(_minValue, _maxValue);
        }
        return result;
    }
}
